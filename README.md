## Participantes
Rogério Amorim Marinho Junior
Thiago Nonato Souza
Tony William Maia de Freitas
Jhonata Leandro Bernardes Paiva

# Equipe de Tutoriais

## Objetivos:
* Criar tutoriais teórico-práticos úteis para o desenvolvimento do projeto
* Requisitos: utilizar jupyter como ferramenta de criação de tutoriais:
    * Exemplos: https://gitlab.com/marceloakira/tutorial

## Grupo 7 - membros
* Rogério Amorim Marinho Junior (LIDER)
* Thiago Nonato Souza (DOCER)
* Tony William Maia de Freitas (DEVER)
* Jhonata Leandro Bernardes Paiva (DOCER)
* Ismael Cirilo Junior (DOCER)

## Tarefas da sprint 1 (02/09 - 08/09): 
* Definir tutoriais para cada membro
* Aprender a utilizar jupyter e revisar tutoriais
