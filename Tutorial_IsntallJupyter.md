#Tutorial para instalar o Jupyter

É necessário possuir o Anaconda(Python) instalado. É possível obter o Anaconda através do link: https://www.anaconda.com/distribution/
Após a instalação estar completa, o Jupyter Notebook já estará instalado, para verificar basta acessar: http://localhost:8888. Também é possível verificar digitando Jupyter Notebook na barra de pesquisa do SO, também pode ser verificado no prompt de comando através de:

...
jupyter notebook
...

